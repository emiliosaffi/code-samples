using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using Foundation;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using static Unity.Mathematics.math;
using static Foundation.math;
using float4x4 = Unity.Mathematics.float4x4;

namespace Framework
{
    public partial class TopologySystem : SystemBase
    {
        const int MaxRays = 2048;

        static TopologySystem instance;
        readonly List<Topology> topologies = new();
        readonly List<List<Topology>> buckets = new();
        readonly Collider[] colliders = new Collider[256];
        byte frame;

        protected override void OnCreate() => instance = this;
        protected override void OnDestroy() => instance = null;

        protected override void OnUpdate()
        {
            using var scheduler = new JobScheduler( Dependency );
            scheduler.Complete();

            scheduler.Fork();
            {
                foreach( var topology in topologies )
                {
                    topology.Samples.Flip();
                    topology.Coverages.Flip();
                    topology.Pose.LShift();

                    int2 shift = topology.CurrentOffset - topology.NextOffset;

                    scheduler.Fork();
                    {
                        scheduler.Push( new ClearSamplesJob
                        {
                            Samples = topology.Samples.next
                        }.Schedule( sq( topology.Size ), topology.Size, scheduler.Concurrent ) );

                        scheduler.Push( new ResetCoveragesJob
                        {
                            Coverages = topology.Coverages.next
                        }.Schedule( topology.Coverages.next.Length, iceil( sqrt( topology.Coverages.next.Length ) ), scheduler.Concurrent ) );
                    }
                    scheduler.Join();

                    scheduler.Push( new CopySamplesJob
                    {
                        Source = topology.Samples.current,
                        Target = topology.Samples.next,
                        Size = topology.Size,
                        Shift = shift
                    }.Schedule( scheduler.Sequential ) );

                    ++frame;
                    int overlapCount = Physics.OverlapBoxNonAlloc(
                        topology.NextPosition - mul( topology.Pose.next.rot, float3( 0.0f, 0.0f, topology.Range.Min ) ),
                        float3( topology.Extent.x2(), topology.Range.Span ),
                        colliders,
                        topology.Pose.next.rot,
                        topology.LayerMask );

                    for( int i = 0; i < overlapCount; ++i )
                    {
                        var collider = colliders[i];
                        var rigidbody = collider.attachedRigidbody;
                        if( !rigidbody )
                            continue;

                        var bounds = collider.bounds;
                        int id = collider.GetInstanceID();
                        if( !topology.Overlaps.TryGetValue( id, out var overlap ) )
                            overlap = new() { Previous = bounds, Current = bounds };

                        if( overlap.Volatile = !rigidbody.IsSleeping() )
                        {
                            overlap.Previous = overlap.Current;
                            overlap.Current = bounds;
                        }

                        overlap.Frame = frame;
                        topology.Overlaps[id] = overlap;
                    }

                    var overlaps = topology.Overlaps.GetKeyValueArrays( Allocator.TempJob );

                    scheduler.Push( new MarkOverlapsJob
                    {
                        Samples = topology.Samples.next,
                        Overlaps = overlaps.Values,
                        Matrix = inverse( float4x4.TRS( topology.NextPosition + mul( topology.Pose.next.rot, float3( -topology.Extent.x2(), 0.0f ) ), topology.Pose.next.rot, 1.0f ) ),
                        Size = topology.Size,
                        Resolution = topology.Resolution,
                        Snapping = topology.Snapping,
                        Frame = frame
                    }.Schedule( overlaps.Length, iceil( sqrt( overlaps.Length ) ), scheduler.Sequential ) );

                    scheduler.Fork();
                    {
                        scheduler.Push( new TrimOverlapsJob
                        {
                            Overlaps = topology.Overlaps,
                            Keys = overlaps.Keys,
                            Values = overlaps.Values,
                            Frame = frame
                        }.Schedule( scheduler.Sequential ) );
                        scheduler.Push( overlaps.Dispose( scheduler.Sequential ) );
                    }
                    scheduler.Part();

                    scheduler.Fork();
                    {
                        for( int i = 0; i <= topology.Subdivisions; ++i )
                        {
                            int subsize = TopologyUtility.GetSize( i );
                            scheduler.Push( new PropagateSamplesJob
                            {
                                Samples = topology.Samples.next,
                                Coverages = topology.Coverages.next,
                                Size = topology.Size,
                                Resolution = topology.Resolution,
                                Subsize = subsize,
                                Subresolution = TopologyUtility.GetResolution( i ),
                                Suboffset = TopologyUtility.GetSubOffset( i )
                            }.Schedule( sq( subsize ), subsize, scheduler.Concurrent ) );
                        }
                    }
                    scheduler.Join();
                }
            }
            scheduler.Join();

            var commands = new NativeArray<SpherecastCommand>( MaxRays, Allocator.TempJob );
            var hits = new NativeArray<RaycastHit>( MaxRays, Allocator.TempJob );
            var count = 0.AsNativeReference( Allocator.TempJob );

            float smoothing = 2.0f;
            scheduler.Fork();
            {
                for( int i = 0; i < buckets.Count; ++i )
                {
                    scheduler.Fork();
                    {
                        foreach( var topology in buckets[i] )
                        {
                            float3x3 rotation = float3x3( topology.Pose.next.rot );

                            int subsize = TopologyUtility.GetSize( i );
                            scheduler.Push( new ScheduleRaycastsJob
                            {
                                Samples = topology.Samples.next,
                                Commands = commands,
                                Count = count,
                                Size = topology.Size,
                                Resolution = topology.Resolution,
                                Subsize = subsize,
                                Subresolution = TopologyUtility.GetResolution( i ),
                                Extent = topology.Extent,
                                Position = topology.NextPosition + rotation.c2 * topology.Range.Min,
                                Rotation = rotation,
                                Radius = topology.Extent / topology.Resolution * smoothing,
                                Distance = topology.Range.Span,
                                LayerMask = topology.LayerMask
                            }.Schedule( sq( subsize ), subsize, scheduler.Concurrent ) );
                        }
                    }
                    scheduler.Join();
                }
            }
            scheduler.Join();

            scheduler.Push( SpherecastCommand.ScheduleBatch( commands, hits, iceil( sqrt( commands.Length ) ), scheduler.Sequential ) );

            scheduler.Fork();
            {
                foreach( var topology in topologies )
                {
                    if( topology.Snapshot )
                        continue;

                    float radius = topology.Extent / topology.Resolution * smoothing;
                    scheduler.Push( new ReadRaycastHitsJob
                    {
                        Samples = topology.Samples.next,
                        Hits = hits,
                        Offset = topology.Range.Min + TopologyUtility.GetLocalPosition( topology.Pose.next ).z,
                        Rotation = inverse( topology.Pose.next.rot )
                    }.Schedule( sq( topology.Size ), topology.Size, scheduler.Concurrent ) );
                }
            }
            scheduler.Join();

            scheduler.Fork();
            {
                scheduler.Push( commands.Dispose( scheduler.Concurrent ) );
                scheduler.Push( hits.Dispose( scheduler.Concurrent ) );
                scheduler.Push( count.Dispose( scheduler.Concurrent ) );
            }
            scheduler.Part();

            scheduler.Fork();
            {
                foreach( var topology in topologies )
                {
                    if( topology.Snapshot )
                        continue;

                    scheduler.Push( new ResetCoveragesJob
                    {
                        Coverages = topology.Coverages.next
                    }.Schedule( topology.Coverages.next.Length, iceil( sqrt( topology.Coverages.next.Length ) ), scheduler.Concurrent ) );

                    scheduler.Fork();
                    {
                        for( int i = 0; i <= topology.Subdivisions; ++i )
                        {
                            int subsize = TopologyUtility.GetSize( i );
                            scheduler.Push( new PropagateSamplesJob
                            {
                                Samples = topology.Samples.next,
                                Coverages = topology.Coverages.next,
                                Size = topology.Size,
                                Resolution = topology.Resolution,
                                Subsize = subsize,
                                Subresolution = TopologyUtility.GetResolution( i ),
                                Suboffset = TopologyUtility.GetSubOffset( i )
                            }.Schedule( sq( subsize ), subsize, scheduler.Concurrent ) );
                        }
                    }
                    scheduler.Join();

                    scheduler.Fork();
                    {
                        for( int i = topology.Subdivisions; i > 0; --i )
                        {
                            int subresolution = TopologyUtility.GetResolution( i );
                            scheduler.Push( new PropagateCoveragesJob
                            {
                                Coverages = topology.Coverages.next,
                                Resolution = TopologyUtility.GetResolution( i - 1 ),
                                Offset = TopologyUtility.GetSubOffset( i - 1 ),
                                Subresolution = subresolution,
                                Suboffset = TopologyUtility.GetSubOffset( i )
                            }.Schedule( sq( subresolution ), subresolution, scheduler.Concurrent ) );
                        }
                    }
                    scheduler.Join();

                    for( int i = 0; i < topology.Subdivisions; ++i )
                    {
                        int subresolution = TopologyUtility.GetResolution( i );
                        scheduler.Push( new InterpolateSamplesJob
                        {
                            Samples = topology.Samples.next,
                            Coverages = topology.Coverages.next,
                            Size = topology.Size,
                            Resolution = topology.Resolution,
                            Subresolution = subresolution,
                            Suboffset = TopologyUtility.GetSubOffset( i )
                        }.Schedule( sq( subresolution ), subresolution, scheduler.Sequential ) );
                    }

                    scheduler.Push( new InterpolateOffsetsJob
                    {
                        Samples = topology.Samples.next,
                        Padding = topology.Padding,
                        MaxSq = sq( 0.5f * World.Time.DeltaTime )
                    }.Schedule( sq( topology.Size ), topology.Size, scheduler.Sequential ) );

                    scheduler.Push( new RelaxOffsetsJob
                    {
                        Samples = topology.Samples.next,
                        Padding = 0.1f,
                        Offset = int2( topology.Size, 1 ),
                        Matrix = mul( float4x4.TRS( topology.CurrentPosition, topology.Pose.current.rot, ( topology.Extent * 2.0f / topology.Resolution ).x2()._3( 1.0f ) ), float4x4.Translate( ( -topology.Resolution / 2.0f ).x2()._3( -TopologyUtility.GetLocalPosition( topology.Pose.current ).z ) ) ),
                        Size = topology.Size,
                        Snapping = topology.Snapping
                    }.Schedule( sq( topology.Size - 1 ), topology.Size - 1, scheduler.Sequential ) );

                    var calculateNormalsJob = new CalculateNormalsJob
                    {
                        Samples = topology.Samples.next,
                        Size = topology.Size,
                        Resolution = topology.Resolution,
                        Scale = topology.Extent * 2.0f / topology.Resolution
                    };

                    calculateNormalsJob.Filter = Topology.Flags.RecalculatePrimaryNormal;
                    calculateNormalsJob.Propagate = Topology.Flags.RecalculateSecondaryNormal;
                    calculateNormalsJob.Reset = ~Topology.Flags.None;
                    scheduler.Push( calculateNormalsJob.Schedule( sq( topology.Size ), topology.Size, scheduler.Sequential ) );

                    calculateNormalsJob.Filter = Topology.Flags.RecalculateSecondaryNormal;
                    calculateNormalsJob.Propagate = Topology.Flags.None;
                    calculateNormalsJob.Reset = ~Topology.Flags.RecalculateNormal;
                    scheduler.Push( calculateNormalsJob.Schedule( sq( topology.Size ), topology.Size, scheduler.Sequential ) );
                }
            }
            scheduler.Join();

            Dependency = scheduler.Handle;
        }

        #region API

        public static byte Frame => instance?.frame ?? 0;

        public static JobHandle GetDependency() => instance?.Dependency ?? default;
        public static void Complete() => instance?.Dependency.Complete();

        public static void Register( Topology topology )
        {
            Complete();
            instance.topologies.Add( topology );
            for( int i = 0; i <= topology.Subdivisions; ++i )
            {
                if( i >= instance.buckets.Count )
                    instance.buckets.Add( new() );
                instance.buckets[i].Add( topology );
            }
        }

        public static void Deregister( Topology topology )
        {
            Complete();
            instance?.topologies.Remove( topology );
            for( int i = 0; i <= topology.Subdivisions; ++i )
                instance?.buckets[i].Remove( topology );
        }

        #endregion

        #region Utility

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        static void Propagate( int pending, in NativeArray<Topology.Coverage> coverages, int2 xy, int resolution, int offset )
        {
            if( all( xy >= 0 ) && all( xy < resolution ) )
                Interlocked.Add( ref coverages.Ref( TopologyUtility.GetIndex( xy, resolution ) + offset ).Pending, pending );
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        static void PropagateAdjacent( int pending, NativeArray<Topology.Coverage> coverages, int2 xy, int resolution, int offset )
        {
            Propagate( pending, coverages, int2( xy.x - 1, xy.y - 1 ), resolution, offset );
            Propagate( pending, coverages, int2( xy.x - 1, xy.y ), resolution, offset );
            Propagate( pending, coverages, int2( xy.x, xy.y - 1 ), resolution, offset );
            Propagate( pending, coverages, xy, resolution, offset );
        }

        #endregion
    }
}
