using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using Foundation;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using static Unity.Mathematics.math;
using static Foundation.math;
using float4x4 = Unity.Mathematics.float4x4;

namespace Framework
{
    public partial class TopologySystem : SystemBase
    {
        [BurstCompile]
        struct ResetCoveragesJob : IJobParallelFor
        {
            public NativeArray<Topology.Coverage> Coverages;
            public void Execute( int index ) => Coverages.Ref( index ) = default;
        }

        [BurstCompile]
        struct ClearSamplesJob : IJobParallelFor
        {
            public NativeArray<Topology.Sample> Samples;
            public void Execute( int index ) => Samples.Ref( index ) = Topology.Sample.Identity;
        }

        [BurstCompile]
        struct CopySamplesJob : IJob
        {
            [ReadOnly]
            public NativeArray<Topology.Sample> Source;
            [WriteOnly]
            public NativeArray<Topology.Sample> Target;
            public int Size;
            public int2 Shift;

            public void Execute()
            {
                if( all( Shift == 0 ) )
                    Target.CopyFrom( Source );
                else
                    Target.CopyFrom( Source, Size, Shift );
            }
        }

        [BurstCompile]
        struct MarkOverlapsJob : IJobParallelFor
        {
            [NativeDisableContainerSafetyRestriction]
            public NativeArray<Topology.Sample> Samples;
            [ReadOnly]
            public NativeArray<TopologyOverlap> Overlaps;
            public float4x4 Matrix;
            public int Size;
            public int Resolution;
            public float Snapping;
            public byte Frame;

            public void Execute( int index )
            {
                ref readonly var overlap = ref Overlaps.RefReadonly( index );
                if( overlap.Frame != Frame )
                    Mark( overlap.Current );
                else if( overlap.Volatile )
                {
                    if( overlap.Previous != overlap.Current )
                        Mark( overlap.Previous );
                    Mark( overlap.Current );
                }
            }

            void Mark( in Bounds bounds )
            {
                float3 extents = bounds.extents;
                float2x2 area = default;

                void encompass( in float4x4 m, float3 p )
                {
                    p = mulv( m, p );
                    area.c0 = min( area.c0, p.xy );
                    area.c1 = max( area.c1, p.xy );
                }

                encompass( Matrix, float3( extents.x, extents.y, extents.z ) );
                encompass( Matrix, float3( -extents.x, extents.y, extents.z ) );
                encompass( Matrix, float3( extents.x, -extents.y, extents.z ) );
                encompass( Matrix, float3( -extents.x, -extents.y, extents.z ) );
                encompass( Matrix, float3( extents.x, extents.y, -extents.z ) );
                encompass( Matrix, float3( -extents.x, extents.y, -extents.z ) );
                encompass( Matrix, float3( extents.x, -extents.y, -extents.z ) );
                encompass( Matrix, float3( -extents.x, -extents.y, -extents.z ) );

                area += mulp( Matrix, bounds.center ).xy.x2();
                area /= Snapping;
                int2x2 region = int2x2(
                    clamp( ifloor( area.c0 ), 0, Resolution ),
                    clamp( iceil( area.c1 ), 0, Resolution ) );

                for( int x = region.c0.x; x <= region.c1.x; ++x )
                for( int y = region.c0.y; y <= region.c1.y; ++y )
                {
                    ref var sample = ref Samples.Ref( TopologyUtility.GetIndex( int2( x, y ), Size ) );
                    sample.Flags |= Topology.Flags.ScheduleRaycast;
                }
            }
        }

        [BurstCompile]
        struct TrimOverlapsJob : IJob
        {
            public NativeParallelHashMap<int, TopologyOverlap> Overlaps;
            [ReadOnly]
            public NativeArray<int> Keys;
            [ReadOnly]
            public NativeArray<TopologyOverlap> Values;
            public byte Frame;

            public void Execute()
            {
                for( int i = 0, i_ = Keys.Length; i < i_; ++i )
                {
                    if( Values[i].Frame != Frame )
                        Overlaps.Remove( Keys[i] );
                }
            }
        }
    }
}
