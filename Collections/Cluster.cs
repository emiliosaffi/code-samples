using System;
using System.Collections;
using System.Collections.Generic;

namespace Foundation
{
    public interface ICluster : IEnumerable
    {
        int Size { get; }
    }

    public interface ICluster<T> : ICluster, IEnumerable<T>
    {
        T this[ int i ] { get; set; }
    }

    public static class Cluster
    {
        public static IEnumerator<T> Enumerate<T>( this ICluster<T> cluster )
        {
            for( int i = 0; i < cluster.Size; ++i )
                yield return cluster[i];
        }
    }
}
