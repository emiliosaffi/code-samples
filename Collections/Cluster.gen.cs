﻿// Generated: /Users/emiliosaffi/r/persea/mori/Assets/Modules/Foundation/Source/Collections/Cluster.tt

using System;
using System.Collections;
using System.Collections.Generic;
using static Unity.Mathematics.math;

namespace Foundation
{
    [Serializable]
    public struct Cluster1<T> : ICluster<T>
    {
        public T _0;
        public int Size => 1;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster1( T source ) : this() => _0 = source;
        public Cluster1( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster1<T>( T source ) => new( source );
        public static implicit operator Cluster1<T>( T[] source ) => new( source );
        public static implicit operator Cluster1<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster2<T> : ICluster<T>
    {
        public T _0, _1;
        public int Size => 2;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster2( T source ) : this() => _0 = _1 = source;
        public Cluster2( T s0, T s1 ) : this() { _0 = s0; _1 = s1; }
        public Cluster2( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster2<T>( T source ) => new( source );
        public static implicit operator Cluster2<T>( T[] source ) => new( source );
        public static implicit operator Cluster2<T>( (T, T) s ) => new( s.Item1, s.Item2 );
        public static implicit operator Cluster2<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster3<T> : ICluster<T>
    {
        public T _0, _1, _2;
        public int Size => 3;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster3( T source ) : this() => _0 = _1 = _2 = source;
        public Cluster3( T s0, T s1, T s2 ) : this() { _0 = s0; _1 = s1; _2 = s2; }
        public Cluster3( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster3<T>( T source ) => new( source );
        public static implicit operator Cluster3<T>( T[] source ) => new( source );
        public static implicit operator Cluster3<T>( (T, T, T) s ) => new( s.Item1, s.Item2, s.Item3 );
        public static implicit operator Cluster3<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster4<T> : ICluster<T>
    {
        public T _0, _1, _2, _3;
        public int Size => 4;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster4( T source ) : this() => _0 = _1 = _2 = _3 = source;
        public Cluster4( T s0, T s1, T s2, T s3 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; }
        public Cluster4( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster4<T>( T source ) => new( source );
        public static implicit operator Cluster4<T>( T[] source ) => new( source );
        public static implicit operator Cluster4<T>( (T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4 );
        public static implicit operator Cluster4<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster5<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4;
        public int Size => 5;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster5( T source ) : this() => _0 = _1 = _2 = _3 = _4 = source;
        public Cluster5( T s0, T s1, T s2, T s3, T s4 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; }
        public Cluster5( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster5<T>( T source ) => new( source );
        public static implicit operator Cluster5<T>( T[] source ) => new( source );
        public static implicit operator Cluster5<T>( (T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5 );
        public static implicit operator Cluster5<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster6<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5;
        public int Size => 6;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster6( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = source;
        public Cluster6( T s0, T s1, T s2, T s3, T s4, T s5 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; }
        public Cluster6( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster6<T>( T source ) => new( source );
        public static implicit operator Cluster6<T>( T[] source ) => new( source );
        public static implicit operator Cluster6<T>( (T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6 );
        public static implicit operator Cluster6<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster7<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6;
        public int Size => 7;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster7( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = source;
        public Cluster7( T s0, T s1, T s2, T s3, T s4, T s5, T s6 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; }
        public Cluster7( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster7<T>( T source ) => new( source );
        public static implicit operator Cluster7<T>( T[] source ) => new( source );
        public static implicit operator Cluster7<T>( (T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7 );
        public static implicit operator Cluster7<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster8<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7;
        public int Size => 8;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster8( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = source;
        public Cluster8( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; }
        public Cluster8( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster8<T>( T source ) => new( source );
        public static implicit operator Cluster8<T>( T[] source ) => new( source );
        public static implicit operator Cluster8<T>( (T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8 );
        public static implicit operator Cluster8<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster9<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8;
        public int Size => 9;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster9( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = source;
        public Cluster9( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; }
        public Cluster9( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster9<T>( T source ) => new( source );
        public static implicit operator Cluster9<T>( T[] source ) => new( source );
        public static implicit operator Cluster9<T>( (T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9 );
        public static implicit operator Cluster9<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster10<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8, _9;
        public int Size => 10;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, 9 => _9, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; case 9: _9 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster10( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = _9 = source;
        public Cluster10( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8, T s9 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; _9 = s9; }
        public Cluster10( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster10<T>( T source ) => new( source );
        public static implicit operator Cluster10<T>( T[] source ) => new( source );
        public static implicit operator Cluster10<T>( (T, T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9, s.Item10 );
        public static implicit operator Cluster10<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster11<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10;
        public int Size => 11;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, 9 => _9, 10 => _10, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; case 9: _9 = value; break; case 10: _10 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster11( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = _9 = _10 = source;
        public Cluster11( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8, T s9, T s10 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; _9 = s9; _10 = s10; }
        public Cluster11( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster11<T>( T source ) => new( source );
        public static implicit operator Cluster11<T>( T[] source ) => new( source );
        public static implicit operator Cluster11<T>( (T, T, T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9, s.Item10, s.Item11 );
        public static implicit operator Cluster11<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster12<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11;
        public int Size => 12;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, 9 => _9, 10 => _10, 11 => _11, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; case 9: _9 = value; break; case 10: _10 = value; break; case 11: _11 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster12( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = _9 = _10 = _11 = source;
        public Cluster12( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8, T s9, T s10, T s11 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; _9 = s9; _10 = s10; _11 = s11; }
        public Cluster12( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster12<T>( T source ) => new( source );
        public static implicit operator Cluster12<T>( T[] source ) => new( source );
        public static implicit operator Cluster12<T>( (T, T, T, T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9, s.Item10, s.Item11, s.Item12 );
        public static implicit operator Cluster12<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster13<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12;
        public int Size => 13;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, 9 => _9, 10 => _10, 11 => _11, 12 => _12, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; case 9: _9 = value; break; case 10: _10 = value; break; case 11: _11 = value; break; case 12: _12 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster13( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = _9 = _10 = _11 = _12 = source;
        public Cluster13( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8, T s9, T s10, T s11, T s12 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; _9 = s9; _10 = s10; _11 = s11; _12 = s12; }
        public Cluster13( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster13<T>( T source ) => new( source );
        public static implicit operator Cluster13<T>( T[] source ) => new( source );
        public static implicit operator Cluster13<T>( (T, T, T, T, T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9, s.Item10, s.Item11, s.Item12, s.Item13 );
        public static implicit operator Cluster13<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster14<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13;
        public int Size => 14;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, 9 => _9, 10 => _10, 11 => _11, 12 => _12, 13 => _13, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; case 9: _9 = value; break; case 10: _10 = value; break; case 11: _11 = value; break; case 12: _12 = value; break; case 13: _13 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster14( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = _9 = _10 = _11 = _12 = _13 = source;
        public Cluster14( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8, T s9, T s10, T s11, T s12, T s13 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; _9 = s9; _10 = s10; _11 = s11; _12 = s12; _13 = s13; }
        public Cluster14( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster14<T>( T source ) => new( source );
        public static implicit operator Cluster14<T>( T[] source ) => new( source );
        public static implicit operator Cluster14<T>( (T, T, T, T, T, T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9, s.Item10, s.Item11, s.Item12, s.Item13, s.Item14 );
        public static implicit operator Cluster14<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster15<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14;
        public int Size => 15;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, 9 => _9, 10 => _10, 11 => _11, 12 => _12, 13 => _13, 14 => _14, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; case 9: _9 = value; break; case 10: _10 = value; break; case 11: _11 = value; break; case 12: _12 = value; break; case 13: _13 = value; break; case 14: _14 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster15( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = _9 = _10 = _11 = _12 = _13 = _14 = source;
        public Cluster15( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8, T s9, T s10, T s11, T s12, T s13, T s14 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; _9 = s9; _10 = s10; _11 = s11; _12 = s12; _13 = s13; _14 = s14; }
        public Cluster15( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster15<T>( T source ) => new( source );
        public static implicit operator Cluster15<T>( T[] source ) => new( source );
        public static implicit operator Cluster15<T>( (T, T, T, T, T, T, T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9, s.Item10, s.Item11, s.Item12, s.Item13, s.Item14, s.Item15 );
        public static implicit operator Cluster15<T>( Vector<T> source ) => new( (T[])source );
    }
    [Serializable]
    public struct Cluster16<T> : ICluster<T>
    {
        public T _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15;
        public int Size => 16;
        public T this[ int i ]
        {
            get => i switch { 0 => _0, 1 => _1, 2 => _2, 3 => _3, 4 => _4, 5 => _5, 6 => _6, 7 => _7, 8 => _8, 9 => _9, 10 => _10, 11 => _11, 12 => _12, 13 => _13, 14 => _14, 15 => _15, _ => throw new IndexOutOfRangeException() };
            set { switch( i ) { case 0: _0 = value; break; case 1: _1 = value; break; case 2: _2 = value; break; case 3: _3 = value; break; case 4: _4 = value; break; case 5: _5 = value; break; case 6: _6 = value; break; case 7: _7 = value; break; case 8: _8 = value; break; case 9: _9 = value; break; case 10: _10 = value; break; case 11: _11 = value; break; case 12: _12 = value; break; case 13: _13 = value; break; case 14: _14 = value; break; case 15: _15 = value; break; default: throw new IndexOutOfRangeException(); } }
        }
        public Cluster16( T source ) : this() => _0 = _1 = _2 = _3 = _4 = _5 = _6 = _7 = _8 = _9 = _10 = _11 = _12 = _13 = _14 = _15 = source;
        public Cluster16( T s0, T s1, T s2, T s3, T s4, T s5, T s6, T s7, T s8, T s9, T s10, T s11, T s12, T s13, T s14, T s15 ) : this() { _0 = s0; _1 = s1; _2 = s2; _3 = s3; _4 = s4; _5 = s5; _6 = s6; _7 = s7; _8 = s8; _9 = s9; _10 = s10; _11 = s11; _12 = s12; _13 = s13; _14 = s14; _15 = s15; }
        public Cluster16( T[] source ) : this() { for( int i = 0, i_ = min( Size, source.Length ); i < i_; ++i ) this[i] = source[i]; }
        public IEnumerator<T> GetEnumerator() => this.Enumerate();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public static implicit operator Cluster16<T>( T source ) => new( source );
        public static implicit operator Cluster16<T>( T[] source ) => new( source );
        public static implicit operator Cluster16<T>( (T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T) s ) => new( s.Item1, s.Item2, s.Item3, s.Item4, s.Item5, s.Item6, s.Item7, s.Item8, s.Item9, s.Item10, s.Item11, s.Item12, s.Item13, s.Item14, s.Item15, s.Item16 );
        public static implicit operator Cluster16<T>( Vector<T> source ) => new( (T[])source );
    }
}