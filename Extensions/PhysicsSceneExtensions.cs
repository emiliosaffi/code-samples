using System;
using Unity.Mathematics;
using UnityEngine;

namespace Foundation
{
    public static class PhysicsSceneExtensions
    {
        const float overlapOffset = 0.05f;
        static readonly Collider[] colliders = new Collider[32];

        public static int Overlap( this PhysicsScene physics, Collider collider, Collider[] colliders )
        {
            Transform transform = collider.transform;
            int count = 0;
            switch( collider )
            {
                case BoxCollider box:
                    count = physics.OverlapBox(
                        transform.TransformPoint( box.center ),
                        box.size.f() / 2.0f * transform.lossyScale,
                        colliders, transform.rotation );
                    break;

                case CapsuleCollider capsule:
                    Vector3 height = default;
                    height[capsule.direction] = capsule.height / 2.0f - capsule.radius;
                    count = physics.OverlapCapsule(
                        transform.TransformPoint( capsule.center + height ),
                        transform.TransformPoint( capsule.center - height ),
                        capsule.radius, colliders );
                    break;
            }
            return count;
        }

        public static bool ComputeOverlap( this PhysicsScene physics, Collider collider, out RaycastHit hit ) =>
            ComputeOverlap( physics, collider, out hit, out _, out _ );
        public static bool ComputeOverlap( this PhysicsScene physics, Collider collider, out RaycastHit hit, out float3 direction, out float distance )
        {
            int count = Overlap( physics, collider, colliders );

            RaycastHit deepestHit = default;
            float3 deepestDirection = 0.0f;
            float deepestDistance = 0.0f;
            bool overlap = false;

            for( int i = 0; i < count; ++i )
                if( collider.ComputeOverlap( colliders[i], out hit, out direction, out distance ) && distance > deepestDistance )
                {
                    deepestHit = hit;
                    deepestDirection = direction;
                    deepestDistance = distance;
                    overlap = true;
                }

            hit = deepestHit;
            direction = deepestDirection;
            distance = deepestDistance;
            return overlap;
        }

        public static bool ComputeOverlap( this PhysicsScene physics,
            Collider colliderA, float3 positionA, quaternion rotationA,
            Collider colliderB, float3 positionB, quaternion rotationB,
            out RaycastHit hit ) => ComputeOverlap( physics,
            colliderA, positionA, rotationA,
            colliderB, positionB, rotationB,
            out hit, out _, out _ );

        public static bool ComputeOverlap( this PhysicsScene physics,
            Collider colliderA, float3 positionA, quaternion rotationA,
            Collider colliderB, float3 positionB, quaternion rotationB,
            out RaycastHit hit, out float3 direction, out float distance )
        {
            var enabled = ( colliderA.enabled, colliderB.enabled );
            colliderA.enabled = colliderB.enabled = true;

            try
            {
                hit = default;
                bool contact = false;

                if( Physics.ComputePenetration(
                    colliderA, positionA, rotationA,
                    colliderB, positionB, rotationB,
                    out var overlapDirection, out distance ) )
                {
                    float castDistance = distance + overlapOffset;
                    contact = physics.Cast( colliderA, positionA, rotationA, -overlapDirection, -castDistance, 0.0f, out hit );
                    hit.distance += castDistance;
                }

                if( contact )
                    hit.distance = distance - hit.distance + Physics.defaultContactOffset;

                direction = overlapDirection;
                return contact;
            }
            finally { ( colliderA.enabled, colliderB.enabled ) = enabled; }
        }

        public static bool Cast( this PhysicsScene physics,
            Collider collider, float3 position, quaternion rotation,
            float3 direction, float minDistance, float maxDistance, out RaycastHit hit )
        {
            bool cast = Cast( physics, collider, position + direction * minDistance, rotation, direction, maxDistance - minDistance, out hit );
            hit.distance += minDistance;
            return cast;
        }

        public static bool Cast( this PhysicsScene physics,
            Collider collider, float3 position, quaternion rotation,
            float3 direction, float distance, out RaycastHit hit )
        {
            float3 scale = collider.transform.lossyScale;
            var matrix = Matrix4x4.TRS( position, rotation, scale );

            bool enabled = collider.enabled;
            collider.enabled = false;

            try
            {
                switch( collider )
                {
                    case BoxCollider box:
                        return physics.BoxCast(
                            matrix.MultiplyPoint3x4( box.center ),
                            box.size.f() * scale / 2.0f, direction, out hit, rotation, distance );

                    case CapsuleCollider capsule: // TODO: Fix radius and height scaling to match Unity
                        Vector3 height = default;
                        height[capsule.direction] = capsule.height / 2.0f - capsule.radius;
                        return physics.CapsuleCast(
                            matrix.MultiplyPoint3x4( capsule.center + height ),
                            matrix.MultiplyPoint3x4( capsule.center - height ),
                            capsule.radius * math.cmed( scale ), direction, out hit, distance );

                    default: throw new ArgumentOutOfRangeException();
                }
            }
            finally { collider.enabled = enabled; }
        }
    }
}
