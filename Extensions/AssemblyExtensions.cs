using UnityEditor.Compilation;
using static System.IO.Path;
using Assembly = System.Reflection.Assembly;

namespace Foundation
{
    public static class AssemblyExtensions
    {
        public static string Location( this Assembly assembly ) =>
            CompilationPipeline.GetAssemblyDefinitionFilePathFromAssemblyName( assembly.GetName().Name );

        public static string Path( this Assembly assembly, string path = "" ) =>
            GetDirectoryName( assembly.Location() ).Path( path );

        public static string Path( this Assembly assembly, string[] paths ) =>
            GetDirectoryName( assembly.Location() ).Path( paths );
    }
}