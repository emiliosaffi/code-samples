using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Sirenix.Utilities;
using UnityEditor;
using static System.Reflection.BindingFlags;

namespace Foundation
{
    public static class TypeExtensions
    {
        #region Fields

        public static FieldInfo FindField( this Type type, string name ) =>
            type.GetField( name, Instance | Static | Public | NonPublic | FlattenHierarchy ) ??
            type.GetField( $"<{name}>k__BackingField", Instance | Static | Public | NonPublic | FlattenHierarchy ) ??
            type.BaseType?.FindField( name );

        public static FieldInfo[] GetDepthOrderedFields( this Type type, BindingFlags? flags = null ) =>
            ( type.BaseType?.GetDepthOrderedFields() ?? new FieldInfo[0] )
            .Concat( type.GetFields( ( flags ?? Instance | Public ) | DeclaredOnly ) ).ToArray();

        #endregion

        #region Properties

        public static PropertyInfo FindProperty( this Type type, string name ) =>
            type.GetProperty( name, Instance | Static | Public | NonPublic | FlattenHierarchy ) ??
            type.BaseType?.FindProperty( name );

        public static PropertyInfo FindProperty( this Type type, params object[] args ) =>
            type.FindProperty( p => p.GetIndexParameters().All( ( p, i ) => p.ParameterType == args[i].GetType() ) );

        public static PropertyInfo FindProperty( this Type type, Predicate<PropertyInfo> predicate ) =>
            type.GetProperties( Instance | Static | Public | NonPublic ).FirstOrDefault( p => predicate?.Invoke( p ) ?? false ) ??
            type.BaseType?.FindProperty( predicate );

        #endregion

        #region Methods

        public static MethodInfo FindMethod( this Type type, string name ) =>
            type.GetMethod( name, InvokeMethod | Instance | Static | Public | NonPublic | FlattenHierarchy ) ??
            type.BaseType?.FindMethod( name );

        public static MethodInfo FindMethod( this Type type, string name, Type[] args ) => type.FindMethod( m =>
            m.Name.Equals( name, StringComparison.Ordinal ) &&
            m.GetParameters().All( ( p, i ) => i < args.Length ? args[i] == null ? p.ParameterType.IsNullableType() : p.ParameterType.IsAssignableFrom( args[i] ) : p.IsOptional ) );

        public static MethodInfo FindMethod( this Type type, Predicate<MethodInfo> predicate ) =>
            type.GetMethods( InvokeMethod | Instance | Static | Public | NonPublic | FlattenHierarchy ).FirstOrDefault( m => predicate?.Invoke( m ) ?? false ) ??
            type.BaseType?.FindMethod( predicate );

        #endregion

        #region Members

        public static MemberInfo FindMember( this Type type, string name, MemberTypes types ) => type.FindMember( m =>
            types.Any( m.MemberType ) && m.Name.Equals( name, StringComparison.Ordinal ) );

        public static MemberInfo FindMember( this Type type, Predicate<MemberInfo> predicate ) =>
            type.GetMembers( Instance | Static | Public | NonPublic | FlattenHierarchy ).FirstOrDefault( m => predicate?.Invoke( m ) ?? false ) ??
            type.BaseType?.FindMember( predicate );

        #endregion

        #region Nested types

        public static Type FindNestedType( this Type type, string name ) =>
            type.GetNestedType( name, Public | NonPublic | FlattenHierarchy ) ??
            type.BaseType?.FindNestedType( name );

        public static Type FindNestedType( this Type type, Predicate<Type> predicate ) =>
            type.GetNestedTypes( Public | NonPublic | FlattenHierarchy ).FirstOrDefault( t => predicate?.Invoke( t ) ?? false ) ??
            type.BaseType?.FindNestedType( predicate );

        public static IEnumerable<Type> FindNestedTypes<T>( this Type type ) where T : class =>
            type.FindNestedTypes( type => type.IsSubclassOf( typeof( T ) ) );

        public static IEnumerable<Type> FindNestedTypes( this Type type, Predicate<Type> predicate ) =>
            type.GetNestedTypes( Public | NonPublic | FlattenHierarchy )
                .Where( t => predicate?.Invoke( t ) ?? false )
                .Concat( type.BaseType?.FindNestedTypes( predicate ) ?? Enumerable.Empty<Type>() )
                .Distinct();

        #endregion

        #region Declaring types

        public static Type FindDeclaringType<T>( this Type type ) => type.FindDeclaringType( typeof( T ) );
        public static Type FindDeclaringType( this Type type, Type constraint ) =>
            constraint.IsAssignableFrom( type ) ? type : type.DeclaringType?.FindDeclaringType( constraint );

        public static Type FindOutermostDeclaringType( this Type type ) => type.DeclaringType?.FindOutermostDeclaringType() ?? type;

        #endregion

        #region Inheritance

        public static bool IsSubclassOfRecursive( this Type type, Type baseType )
        {
            for( ; type != null && type != typeof( object ); type = type.BaseType )
                if( type == baseType || type.IsGenericType && type.GetGenericTypeDefinition() == baseType )
                    return true;
            return false;
        }

        #endregion

        #region Instances

        public static object Construct( this Type type ) => Activator.CreateInstance( type );
        public static T Construct<T>( this Type type ) => (T)Activator.CreateInstance( type );
        public static object Construct( this Type type, params object[] args ) => Activator.CreateInstance( type, args );
        public static T Construct<T>( this Type type, params object[] args ) => (T)Activator.CreateInstance( type, args );

        #endregion

        #region Attributes

        public static T GetCustomAttribute<T>( this Type type, bool inherit, bool interfaces ) where T : Attribute =>
            type.GetCustomAttribute<T>( inherit ) ?? ( interfaces
                ? type.GetInterfaces().SelectMany( i => i.GetCustomAttributes<T>() ).FirstOrDefault()
                : null );

        public static T[] GetCustomAttributes<T>( this Type type, bool inherit, bool interfaces ) where T : Attribute => ( interfaces
                ? type.GetCustomAttributes<T>( inherit )
                    .Concat( type.GetInterfaces().SelectMany( i => i.GetCustomAttributes<T>() ) )
                : type.GetCustomAttributes<T>( inherit )
            ).ToArray();

        static bool IsAssignableFrom<T>( Type type ) => typeof( T ).IsAssignableFrom( type );

        #endregion
    }
}
