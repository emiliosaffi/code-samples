using Unity.Mathematics;
using UnityEngine;

namespace Foundation
{
    public static class ColliderExtensions
    {
        public static int Overlap( this Collider collider, Collider[] colliders ) =>
            collider.gameObject.scene.GetPhysicsScene().Overlap( collider, colliders );

        public static bool ComputeOverlap( this Collider collider, out RaycastHit hit ) =>
            collider.gameObject.scene.GetPhysicsScene().ComputeOverlap( collider, out hit );
        public static bool ComputeOverlap( this Collider collider, out RaycastHit hit, out float3 direction, out float distance ) =>
            collider.gameObject.scene.GetPhysicsScene().ComputeOverlap( collider, out hit, out direction, out distance );

        public static bool ComputeOverlap( this Collider collider, Collider other, out RaycastHit hit ) => collider.gameObject.scene.GetPhysicsScene().ComputeOverlap(
            collider, collider.transform.position, collider.transform.rotation,
            other, other.transform.position, other.transform.rotation,
            out hit );

        public static bool ComputeOverlap( this Collider collider, Collider other, out RaycastHit hit, out float3 direction, out float distance ) => collider.gameObject.scene.GetPhysicsScene().ComputeOverlap(
            collider, collider.transform.position, collider.transform.rotation,
            other, other.transform.position, other.transform.rotation,
            out hit, out direction, out distance );

        public static bool Cast( this Collider collider, float3 direction, float minDistance, float maxDistance, out RaycastHit hit ) => collider.gameObject.scene.GetPhysicsScene().Cast(
            collider, collider.transform.position, collider.transform.rotation, direction, minDistance, maxDistance, out hit );

        public static bool Cast( this Collider collider, float3 direction, float distance, out RaycastHit hit ) => collider.gameObject.scene.GetPhysicsScene().Cast(
            collider, collider.transform.position, collider.transform.rotation, direction, distance, out hit );
    }
}
