using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Foundation;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEditorInternal;
using UnityEngine;

namespace Conduit
{
    public class CSProjectT4Includer : AssetPostprocessor
    {
        [Serializable]
        struct asmdef
        {
            public string rootNamespace;
            public string[] references;
        }

        static readonly Dictionary<string, string[]> cachedIncludes = new();
        static readonly string[] emptyIncludes = Array.Empty<string>();

        static string OnGeneratedCSProject( string path, string content )
        {
            string assemblyPath = CompilationPipeline.GetAssemblyDefinitionFilePathFromAssemblyName( Path.GetFileNameWithoutExtension( path ) );
            string directoryPath = Path.GetDirectoryName( assemblyPath );

            if( !IsValidPath( directoryPath ) )
                return content;

            return content.Insert( content.Length - "</Project>".Length - 1, "\n" +
                $"{GetAssemblyIncludes( assemblyPath ).Unique().Where( s => !string.IsNullOrEmpty( s ) ).Join( "\n" )}\n" );
        }

        static string[] GetAssemblyIncludes( string assemblyPath )
        {
            if( !IsValidPath( assemblyPath ) )
                return emptyIncludes;

            if( !cachedIncludes.TryGetValue( assemblyPath, out var includes ) )
            {
                var data = JsonUtility.FromJson<asmdef>( AssetDatabase.LoadAssetAtPath<AssemblyDefinitionAsset>( assemblyPath ).text );
                cachedIncludes[assemblyPath] = includes = string.IsNullOrEmpty( data.rootNamespace ) ? emptyIncludes : data.references
                    .Select( CompilationPipeline.GetAssemblyDefinitionFilePathFromAssemblyReference )
                    .SelectMany( GetAssemblyIncludes )
                    .Where( s => !string.IsNullOrEmpty( s ) )
                    .Append( GenerateAssemblyInclude( assemblyPath, data ) )
                    .ToArray();
            }
            return includes;
        }

        static string GenerateAssemblyInclude( string assemblyPath, in asmdef data ) =>
            $"<PropertyGroup><{data.rootNamespace}>$(ProjectDir)/{Path.GetDirectoryName( assemblyPath )}</{data.rootNamespace}></PropertyGroup>";

        static bool IsValidPath( string directoryPath ) => !string.IsNullOrEmpty( directoryPath ) && directoryPath.StartsWith( "Assets/Modules/" );
    }
}
