using System.Linq;
using Foundation;
using Sirenix.Utilities;
using Unity.Mathematics;
using UnityEngine;
using static Weaver.editor;

namespace Palette
{
    public interface IVertexManipulator
    {
        VertexHandle Handle { get; }
        VertexManipulator.Entry[] Entries { get; }

        void Begin( int handleIndex );
        void End();
        void Manipulate( ComputeShader compute, float4? mul = null, float4? add = null );
    }

    [VertexSystem]
    public class VertexManipulator : VertexMode.Feature, IVertexManipulator
    {
        public struct Entry
        {
            public VertexTarget Target;
            public VertexChannel[] Channels;
            public ComputeBuffer[] Sources;
            public int Count;
        }

        [Inject] IVertexTargets targets;
        [Inject] IVertexHandles handles;
        [Inject] IVertexChannels channels;
        [Inject] IVertexSelector selector;
        [Inject] IVertexRenderer renderer;
        [Inject] IVertexModifier modifier;

        public VertexHandle Handle { get; private set; }
        public Entry[] Entries { get; private set; }

        ComputeShader copy;

        public override void OnInit()
        {
            copy = Resources.Load<ComputeShader>( "Vertex/Manipulation/C_Copy" );
        }

        public void Begin( int handleIndex )
        {
            Handle = handles.Buffer[handleIndex];
            Entries = targets.Buffer
                .Select( target =>
                {
                    var availableChannels = target.Channels
                        .Where( channel => channel.Layout.Processing.Handle == Handle.Layout.ID )
                        .ToArray();
                    var channels = Enumerable.Range( 0, 4 ).Select( i => i < availableChannels.Length ? availableChannels[i] : target.MockChannels[i] ).ToArray();

                    return new Entry
                    {
                        Target = target,
                        Channels = channels,
                        Sources = channels.Select( channel =>
                        {
                            var buffer = new ComputeBuffer( channel.Buffer.Length, sizeof( float ) );
                            copy.SetBuffer( 0, "Src", channel.Buffer );
                            copy.SetBuffer( 0, "Dst", buffer );
                            copy.SetInt( "Index", 0 );
                            copy.SetInt( "Length", buffer.count );
                            copy.DispatchAuto( 0, buffer.count );
                            return buffer;
                        } ).ToArray(),
                        Count = availableChannels.Length,
                    };
                } )
                .ToArray();

            undo.Push( $"{Handle} Manipulation", targets, channels, selector, modifier );
            selector.Manipulating = true;
        }

        public void End()
        {
            Entries.ForEach( entry =>
            {
                entry.Channels.ForEach( channel =>
                {
                    selector.Refresh();
                    modifier.Save( entry.Target, channel );
                } );
                entry.Sources.ForEach( origin => origin.Dispose() );
            } );

            Handle = null;
            Entries = null;

            undo.Pop();
            selector.Manipulating = false;
        }

        public void Manipulate( ComputeShader compute, float4? mul = null, float4? add = null )
        {
            compute.SetBuffer( 0, "Selection", selector.Selection );

            compute.SetMatrix( "ViewMatrix", sv.camera.transform.localToWorldMatrix );
            compute.SetVector( "Mul", mul ?? 1.0f );
            compute.SetVector( "Add", add ?? 0.0f );

            foreach( var entry in Entries )
            {
                var mesh = entry.Target.ActiveModification.TargetMesh;

                compute.SetInt( "TargetID", entry.Target.ID );
                compute.SetInt( "Index", 0 );
                compute.SetInt( "Length", mesh.vertexCount );
                compute.SetMatrix( "ObjectMatrix", entry.Target.ToLocal );

                compute.SetBuffer( 0, "Src0", entry.Sources[0] );
                compute.SetBuffer( 0, "Src1", entry.Sources[1] );
                compute.SetBuffer( 0, "Src2", entry.Sources[2] );
                compute.SetBuffer( 0, "Src3", entry.Sources[3] );

                compute.SetBuffer( 0, "Dst0", entry.Channels[0].Buffer );
                compute.SetBuffer( 0, "Dst1", entry.Channels[1].Buffer );
                compute.SetBuffer( 0, "Dst2", entry.Channels[2].Buffer );
                compute.SetBuffer( 0, "Dst3", entry.Channels[3].Buffer );

                compute.DispatchAuto( 0, mesh.vertexCount );

                for( int i = 0; i < entry.Count; i++ )
                {
                    VertexChannel channel = entry.Channels[i];
                    modifier.Modify( entry.Target, channel, true );
                }
            }
        }
    }
}
