using Foundation;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;
using Weaver;
using static Weaver.editor;

namespace Palette
{
    public partial class VertexKeymap
    {
        public Feature<VertexBoxSelection> BoxSelection = M;
    }

    [VertexTool]
    public class VertexBoxSelection : VertexMode.Feature
    {
        public class Keymap : EditorKeymap
        {
            public SelectionDrag Selection;
        }

        [Inject] IVertexViewer viewer;
        [Inject] IVertexSelector selector;
        [Inject] Keymap km;

        EditorFloatPref pointMargin = ( nameof( VertexBoxSelection ), nameof( pointMargin ), 100.0f );

        public override void OnActivate()
        {
            km.Selection.OnRefresh += OnSelectionRefresh;
            km.Selection.OnHover += OnSelectionHover;
            km.Selection.OnDrag += OnSelectionDrag;
            km.Selection.Modifiers.Select.OnChange += OnModifiersChange;
            km.Selection.Modifiers.Deselect.OnChange += OnModifiersChange;
            km.Selection.Drag.OnRelease += OnDragRelease;
            km.Selection.Drag.OnSceneGUI += OnDragSceneGUI;

            selector.Previewing = true;
        }

        public override void OnDeactivate()
        {
            km.Selection.OnRefresh -= OnSelectionRefresh;
            km.Selection.OnHover -= OnSelectionHover;
            km.Selection.OnDrag -= OnSelectionDrag;
            km.Selection.Modifiers.Select.OnChange -= OnModifiersChange;
            km.Selection.Modifiers.Deselect.OnChange -= OnModifiersChange;
            km.Selection.Drag.OnRelease -= OnDragRelease;
            km.Selection.Drag.OnSceneGUI -= OnDragSceneGUI;
        }

        void OnSelectionRefresh()
        {
            selector.Previewing = !km.Selection.Drag;
        }

        void OnSelectionHover( bool hover ) => Hover( hover, km.Selection.Drag && !km.Selection.Drag.Release );

        void OnSelectionDrag()
        {
            selector.Clear( false, true );
            if( km.Selection.Dragging )
            {
                selector.Margin = 0.0f;
                selector.Rect( km.Selection.Drag.Scene.Rect.c );
            }
            else
            {
                selector.Margin = pointMargin;
                selector.Point( km.Selection.Drag.Scene.Origin );
            }
            viewer.Repaint();
        }

        void OnModifiersChange()
        {
            selector.Deselecting = km.Selection.Deselecting;
            selector.Selecting = km.Selection.Selecting;
            selector.Sync();
        }

        void OnDragRelease()
        {
            selector.Apply();
            selector.Margin = pointMargin;
            selector.Sync();
        }

        void OnDragSceneGUI()
        {
            if( !e.OnRepaint() )
                return;

            if( km.Selection.Dragging )
            {
                Handles.BeginGUI();
                GUI.Box( new( km.Selection.Drag.Scene.Origin, km.Selection.Drag.Scene.Delta ), "" );
                Handles.EndGUI();
            }
        }

        void Hover( bool hover, bool retain )
        {
            if( hover )
                Request<IVertexHovering>( h =>
                {
                    h.Margin = pointMargin;
                    h.Radius = 0.5f;
                    h.Retain = false;
                } );
            else
                Discard<IVertexHovering>( h => h.Retain = retain );
        }
    }
}
