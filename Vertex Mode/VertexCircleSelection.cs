using Foundation;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using Weaver;
using static Weaver.editor;
using static Foundation.math;

namespace Palette
{
    public partial class VertexKeymap
    {
        public Feature<VertexCircleSelection> CircleSelection = C;
    }

    [VertexTool]
    public class VertexCircleSelection : VertexMode.Feature
    {
        public class Keymap : EditorKeymap
        {
            public Brush Brush;
            public SelectionModifiers Modifiers;
        }

        [Inject] IVertexSelector selector;
        [Inject] Keymap km;

        BrushCanvasTexture canvas;
        BrushTipTextureProcedural tip;

        static EditorFloatPref margin = ( nameof( VertexCircleSelection ), nameof( margin ), 0.0f );

        public override void OnActivate()
        {
            km.Brush.Context.Bind( EditorGUIUtility.PixelsToPoints( sv.camera.pixelRect ) );
            km.Brush.Context.Bind( GraphicsFormat.R32G32B32A32_SFloat );

            canvas = km.Brush.Bind( new BrushCanvasTexture
            {
                Layers = new BrushCanvasTexture.Layer[] { ( "Brush/Paint/C_DepthToAlpha", true ) },
                Depthed = true,
                Hidden = true
            } );

            tip = km.Brush.Bind( new BrushTipTextureProcedural { Size = 24.0f } );

            km.Brush.Bind<BrushCursorTextureProcedural>();
            km.Brush.Bind( new BrushStrokeSmoothed
            {
                Materials = new Material[] { new( Shader.Find( "Palette/Brush/Stroke/DepthMax" ) ) },
                Spacing = 0.1f,
                Retain = true
            } );

            km.Modifiers.Select.OnChange += OnModifiersChange;
            km.Modifiers.Deselect.OnChange += OnModifiersChange;
            km.Brush.OnHover += OnBrushHover;
            km.Brush.OnReshape += OnBrushReshape;
            km.Brush.Paint.OnPress += OnPaintPress;
            km.Brush.Paint.OnRelease += OnPaintRelease;
            km.Brush.OnPaint += OnPaint;

            Hover( true, false );
        }

        public override void OnDeactivate()
        {
            km.Modifiers.Select.OnChange -= OnModifiersChange;
            km.Modifiers.Deselect.OnChange -= OnModifiersChange;
            km.Brush.OnHover -= OnBrushHover;
            km.Brush.OnReshape -= OnBrushReshape;
            km.Brush.Paint.OnPress -= OnPaintPress;
            km.Brush.Paint.OnRelease -= OnPaintRelease;
            km.Brush.OnPaint -= OnPaint;

            Hover( false, true );
        }

        public override void OnSceneGUI() =>
            canvas.Resize( iround( EditorGUIUtility.PixelsToPoints( sv.camera.pixelRect.size ) ) );

        void OnModifiersChange()
        {
            selector.Deselecting = km.Modifiers.Deselect;
            selector.Selecting = km.Modifiers.Select;
            selector.Sync();
        }

        void OnBrushHover( bool hover )
        {
            selector.Previewing = hover;
            Hover( hover, km.Brush.Paint );
        }

        void OnBrushReshape() => Hover( true, false );

        void OnPaintPress() => undo.Push( "Circle Selection", selector );

        void OnPaintRelease()
        {
            canvas.Clear();
            selector.Apply();
            selector.Margin = margin;
            selector.Sync();
            undo.Pop();
        }

        void OnPaint( rect2 rect, float2 _ )
        {
            selector.Texture( rect, canvas.Target.color );
            sv.Repaint();
        }

        void Hover( bool hover, bool retain )
        {
            if( hover )
                Request<IVertexHovering>( h =>
                {
                    h.Margin = margin;
                    h.Retain = retain;
                    h.Texture = () => tip.Texture;
                } );
            else
                Discard<IVertexHovering>( h => h.Retain = retain );
        }
    }
}
