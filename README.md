# Code samples
Below are short descriptions detailing the contents of each source folder. 

## Collections
Contains an example of how I use T4 to facilitate code generation to circumvent C# limitations.
Cluster is a fixed size array used primarily to pass data without GC.

## Extensions
Showcases how I usually structure code using C# extensions.

## T4
The heart of the T4 generation pipeline. Some lines need to be injected into the .csproj file to enable T4 and to facilitate code reusability through file inclusion by binding namespace keywords to directory paths.

## Topology
Shows the main loop and some of the simpler jobs of the TopologySystem as showcased in the following tweet:
https://twitter.com/EmilioSaffi/status/1705127915166617935

## Vertex Mode
Samples from a proprietary vertex editing tool for Unity. Uses a custom built context for dependency injection and is mostly driven by compute shaders, allowing it to handle a large amount of geometric complexity.